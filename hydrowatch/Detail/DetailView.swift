//
//  DetailView.swift
//  hydrowatch
//
//  Created by Yosua Marchel on 02/11/20.
//

import UIKit

class DetailView: UIView {

    @IBOutlet weak var plantNameLabel: UILabel!
    @IBOutlet weak var plantAgeLabel: UILabel!
    @IBOutlet weak var harvestDayLabel: UILabel!
    
    @IBOutlet weak var waterLevelLabel: UILabel!
    @IBOutlet weak var phLevelLabel: UILabel!
    @IBOutlet weak var ppmLevelLabel: UILabel!
    @IBOutlet weak var temperatureLevelLabel: UILabel!
    
    @IBOutlet weak var waterSensorColorView: UIView!
    @IBOutlet weak var phSensorColorView: UIView!
    @IBOutlet weak var ppmSensorColorView: UIView!
    @IBOutlet weak var temperatureSensorColorView: UIView!
    
    @IBOutlet weak var sensorTimerLabel: UILabel!
    @IBOutlet weak var warningLabel: UILabel!
    @IBOutlet weak var suggestionLabel: UILabel!
    
    @IBOutlet weak var plantImage: UIImageView!
    @IBOutlet weak var harvestButton: UIButton!
        
    override func awakeFromNib() {
        super.awakeFromNib()
        setInitialView()
    }
    
    private func setInitialView(){
        waterSensorColorView.layer.cornerRadius = 10
        phSensorColorView.layer.cornerRadius = 10
        ppmSensorColorView.layer.cornerRadius = 10
        temperatureSensorColorView.layer.cornerRadius = 10
        
        harvestButton.layer.cornerRadius = 10
        
        self.backgroundColor = UIColor.white
        
        suggestionLabel.isHidden = true
        warningLabel.isHidden = true
    }
    
    
    
    public func setLabelValue(with plant: String, harvest: String) {
        plantNameLabel.text = plant
        harvestDayLabel.text = harvest
    }
    
}
