//
//  DetailViewController.swift
//  hydrowatch
//
//  Created by Yosua Marchel on 02/11/20.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet weak var detailView: DetailView!
    
    var data: DataContainer?
    var vSpinner : UIView?
    
    @IBOutlet weak var hiddenView: UIView!
    @IBOutlet weak var plantAge: UILabel!
    @IBOutlet weak var pH: UILabel!
    @IBOutlet weak var ppm: UILabel!
    @IBOutlet weak var lastUpdate: UILabel!
    
    var plant: String!
    var harvest: String!
    
    func getAge() -> Int {
        let todayDate = Date()
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMMM yyyy"
        let date = formatter.date(from: data!.createdDate)!
        
        let diff = Calendar.current.dateComponents([.day], from: date, to: todayDate).day!
        
        return diff
    }
    
    //dummy data
    var phLevel: Int = 100
    var ppmLevel: Int = 100
    var waterLevel: Int = 100
    var waterTemp: Int = 25
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var message: String = ""
        
        if data!.dayTilHarvest == 0 {
            message = "Harvestable now!"
        } else {
            message = "\(data!.dayTilHarvest) Days until Harvest"
        }
        
        plant = data?.plantName
        harvest = message
        detailView.setLabelValue(with: plant, harvest: harvest)
        
        self.showSpinner(onView: self.view)
        
        plantAge.text = data?.createdDate
        plantAge.text = "\(getAge()) days"
        
        print(data!.plantId)
                
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Edit",
                                                            style: .plain,
                                                            target: self,
                                                            action: #selector(toEdit))
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        detailView.addGestureRecognizer(tap)
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        let url = URL(string: "https://hydrowatch.herokuapp.com/api/plant/get-data?plantId=\(data!.plantId)")!
//
//        URLSession.shared.dataTask(with: url) { data, response, error in
//          if let data = data {
//              do {
//                let res = try JSONDecoder().decode(ResponseData.self, from: data)
//                DispatchQueue.main.async {
//                    self.pH.text = res.ph
//                    self.ppm.text = res.ppm
//                    self.lastUpdate.text = "Last Update: \(String(describing: res.createdAt!))"
//                    self.removeSpinner()
//                    self.hiddenView.isHidden = true
//                }
//              } catch let error {
//                print(error)
//              }
//           }
//       }.resume()
        
        handleTap()
    }
    
    @IBAction func harvestNow(_ sender: Any) {
        let alert = UIAlertController(title: "Warning", message: "Would you like to delete this container?", preferredStyle: UIAlertController.Style.alert)

        alert.addAction(UIAlertAction(title: "Delete", style: UIAlertAction.Style.default, handler: {(UIAlertAction) -> Void in
            DeleteContainerData(plantId: self.data!.plantId)
            self.navigationController?.popToRootViewController(animated: true)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil))

        self.present(alert, animated: true, completion: nil)
    }
    
    @objc public func toEdit() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "CreateContainerVC") as! CreateContainerVC
        vc.plantId = data!.plantId
        vc.isEdit = true
        vc.data = data
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        phLevel -= 1
        ppmLevel -= 2
        waterLevel -= 1
        waterTemp += 1
        pH.text = String(phLevel)
        ppm.text = String(ppmLevel)
        detailView.waterLevelLabel.text = String(waterLevel) + "%"
        detailView.temperatureLevelLabel.text = String(waterTemp)
                            self.removeSpinner()
                            self.hiddenView.isHidden = true
        if waterTemp >= 39 {
            detailView.temperatureSensorColorView.backgroundColor = #colorLiteral(red: 0.88661623, green: 0.3861777782, blue: 0.3883196712, alpha: 1)
            detailView.suggestionLabel.isHidden = false
            detailView.warningLabel.isHidden = false
            detailView.plantImage.image = UIImage(named: "PNG Plant - Hot")
        }else{
            detailView.temperatureSensorColorView.backgroundColor = #colorLiteral(red: 0.3923841119, green: 0.8585931063, blue: 0.5606201291, alpha: 1)
            detailView.suggestionLabel.isHidden = true
            detailView.warningLabel.isHidden = true
            detailView.plantImage.image = UIImage(named: "PNG Plant - Good")
        }
    }
}

extension DetailViewController {
    func showSpinner(onView : UIView) {
        let spinnerView = UIView.init(frame: onView.bounds)
        spinnerView.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
        let ai = UIActivityIndicatorView.init(style: .whiteLarge)
        ai.startAnimating()
        ai.center = spinnerView.center
        
        DispatchQueue.main.async {
            spinnerView.addSubview(ai)
            onView.addSubview(spinnerView)
        }
        
        vSpinner = spinnerView
    }

    func removeSpinner() {
        DispatchQueue.main.async {
            self.vSpinner?.removeFromSuperview()
            self.vSpinner = nil
        }
    }
}
