//
//  ViewController.swift
//  hydrowatch
//
//  Created by Dicky Geraldi on 22/10/20.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    @IBAction func goScan(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "scan") as! ScanQR
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

