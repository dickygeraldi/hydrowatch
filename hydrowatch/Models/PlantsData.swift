//
//  PlantsData.swift
//  hydrowatch
//
//  Created by Dicky Geraldi on 23/10/20.
//

import Foundation
import UIKit

struct PlantsData: Codable {
    var PPM: Int
    var Ph: Int
    var WaterLevel: Int
}

struct DataContainer {
    var containerName: String
    var plantId: String
    var plantName: String
    var createdDate: String
    var updatedAt: String
    var dayTilHarvest: Int16
    var sensorStatus: UIImage
}

func GetDate() -> String {
    let today = Date()
    let formatter1 = DateFormatter()
    formatter1.dateFormat = "dd/MM/yyyy, HH:mm:ss"
    
    return formatter1.string(from: today)
}

struct ResponseData: Codable {
    let message: String
    let plantId: String?
    let ppm: String?
    let ph: String?
    let waterLevel: String?
    let createdAt: String?
}
