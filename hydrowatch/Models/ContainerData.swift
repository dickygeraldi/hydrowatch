//
//  Container.swift
//  Macro Dashboard
//
//  Created by Yosua Marchel on 22/10/20.
//  Copyright © 2020 Yosua Marchel. All rights reserved.
//

import Foundation
import UIKit

//class Container {
//
//    var name: [String] = ["Backyard Plants", "Balcony floor2", "Secret lair garden"]
//    var plantType: [String] = ["Tomato", "Bok Choi", "Celery"]
//    var day: [String] = ["20 Days until Harvest", "10 Days until Harvest", "Harvestable now!"]
//    var sensorStatus: [UIImage] = [#imageLiteral(resourceName: "Alert - Green"), #imageLiteral(resourceName: "Alert - Red"), #imageLiteral(resourceName: "Alert - Harvest")]
//
//}

struct Container {
    let name: String
    let plantType: String
    let day: String
    let sensorStatus: UIImage
}
