//
//  ViewController.swift
//  Macro Dashboard
//
//  Created by Yosua Marchel on 22/10/20.
//  Copyright © 2020 Yosua Marchel. All rights reserved.
//

import UIKit

class DashboardViewController: UIViewController {
    
    @IBOutlet weak var dashboardView: DashboardView!
    @IBOutlet weak var goScan: UIButton!
    
    public var data: [DataContainer] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dashboardView.tableView.delegate = self
        dashboardView.tableView.dataSource = self
        
        data = GetContainerData()
//        print(data[0].dayTilHarvest)
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        data = GetContainerData()
        dashboardView.tableView.reloadData()
        if data.count != 0 {
            dashboardView.arrowImage.isHidden = true
            dashboardView.noContainerLabel.isHidden = true
            dashboardView.tableView.isHidden = false
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let controller = segue.destination as? DetailViewController {
            guard let indexPath = sender as? IndexPath else { return }
            controller.data = data[indexPath.row]
            controller.title = data[indexPath.row].containerName
        }
    }
    
    @IBAction func goScanQr(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "scan") as! ScanQR
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

// MARK:- Table View Cell Base Protocol
extension DashboardViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContainerCell", for: indexPath) as! DashboardTableViewCell
        var message: String = ""
        
        print(data[indexPath.row].dayTilHarvest)
        
        if data[indexPath.row].dayTilHarvest == 0 {
            message = "Harvestable now!"
        } else {
            message = "\(data[indexPath.row].dayTilHarvest) Days until Harvest"
        }
        
        cell.cellConfig(withName: data[indexPath.row].containerName,
                        type: data[indexPath.row].plantName,
                        day: message,
                        sensor: data[indexPath.row].sensorStatus)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        performSegue(withIdentifier: "toDetail", sender: indexPath)
    }
}
