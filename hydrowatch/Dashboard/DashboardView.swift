//
//  DashboardView.swift
//  Macro Dashboard
//
//  Created by Yosua Marchel on 22/10/20.
//  Copyright © 2020 Yosua Marchel. All rights reserved.
//

import UIKit

class DashboardView: UIView {
    
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var arrowImage: UIImageView!
    @IBOutlet weak var noContainerLabel: UILabel!
    
    var dasboardVC: DashboardViewController!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setInitialView()
    }
    
    private func setInitialView(){
        arrowImage.isHidden = false
        noContainerLabel.isHidden = false
        tableView.isHidden = true
        self.backgroundColor = UIColor.white
    }
    
    @IBAction func addButtonClicked(_ sender: Any) {
        
    }
    
}
