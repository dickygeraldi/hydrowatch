//
//  ChoosePlant.swift
//  hydrowatch
//
//  Created by Dicky Geraldi on 27/10/20.
//

import UIKit

class ChoosePlant: UIViewController {

    var plantType: [String] = ["Bak Choi", "Celery", "Chili", "Tomato", "Kale"]
    var index: Int = 0
    
    @IBOutlet weak var plantData: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let navBar = UINavigationBar(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 44))
        view.addSubview(navBar)

        let navItem = UINavigationItem(title: "Plant Type")
        navItem.rightBarButtonItem = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(Done))

        navBar.setItems([navItem], animated: false)
        
        plantData.delegate = self
        plantData.dataSource = self
        plantData.separatorStyle = .none
    }
    
    @objc func Done() {
        self.performSegue(withIdentifier: "SendChoosenPlant", sender: self)
    }
}

extension ChoosePlant: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return plantType.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let data = plantType[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "PlantData") as! PlantDataCell

        cell.plantData.text = data
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        index = indexPath.row
    }
}

extension ChoosePlant {
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destination = segue.destination as! CreateContainerVC
        destination.choosenPlant.text = plantType[index]
    }
}
