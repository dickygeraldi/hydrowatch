//
//  PlantDataCell.swift
//  hydrowatch
//
//  Created by Dicky Geraldi on 27/10/20.
//

import UIKit

class PlantDataCell: UITableViewCell {

    @IBOutlet weak var plantData: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
