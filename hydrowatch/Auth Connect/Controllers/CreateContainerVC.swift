//
//  CreateContainerVC.swift
//  hydrowatch
//
//  Created by Dicky Geraldi on 26/10/20.
//

import UIKit

class CreateContainerVC: UIViewController {

    var plantId : String = ""
    var plantChoosen: String = ""
    var isEdit: Bool = false
    var data: DataContainer?
    
    @IBOutlet weak var containerTextField: UITextField!
    @IBOutlet weak var imageButton: UIImageView!
    @IBOutlet weak var plantDate: UITextField!
    @IBOutlet weak var choosenPlant: UILabel!
    
    let locale = Locale.preferredLanguages.first
    let datePicker = UIDatePicker()
    
    let formatter = DateFormatter()
    
    func setUpStartDate() {
        plantDate.inputView = datePicker
        datePicker.preferredDatePickerStyle = .inline
        
        datePicker.locale = Locale(identifier: locale!)
        
        datePicker.maximumDate = Date()
        
        datePicker.addTarget(self, action: #selector(startPickerChange), for: .valueChanged)
    }
    
    func setUpEditview() {
        self.navigationItem.title = "Edit Container"
        self.navigationController?.navigationItem.title = "Back"
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(editNow))
        
        containerTextField.text = data?.containerName
        choosenPlant.text = data?.plantName
        plantDate.text = data?.createdDate
        plantDate.isEnabled = false
    }
    
    func setUpCreateView() {
        self.navigationItem.title = "Create Container"
        self.navigationController?.navigationItem.title = "Back"
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Create", style: .plain, target: self, action: #selector(createNow))
    }
    
    @objc func startPickerChange() {
        getDateFromPicker(sender: datePicker)
    }
    
    func getDateFromPicker(sender: UIDatePicker) {
        formatter.dateFormat = "dd MMMM yyyy"
        if sender == datePicker {
            plantDate.text = formatter.string(from: datePicker.date)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let singleTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.goChoosePlant))
        
        imageButton.addGestureRecognizer(singleTap)
        imageButton.isUserInteractionEnabled = true
        
        setUpStartDate()
        initializeHideKeyboard()
        
        if isEdit == true {
            setUpEditview()
        } else {
            setUpCreateView()
        }
    }
}

extension CreateContainerVC {
    func initializeHideKeyboard(){
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(dismissMyKeyboard))
        
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissMyKeyboard(){
        view.endEditing(true)
    }
    
    @IBAction func unwindToChoose(_ unwindSegue: UIStoryboardSegue) {}
    
    @objc func createNow() {
        formatter.dateFormat = "dd MMMM yyyy"
        
        var data: DataContainer = DataContainer(containerName: "", plantId: "", plantName: "", createdDate: "", updatedAt: "", dayTilHarvest: 0, sensorStatus: #imageLiteral(resourceName: "Alert - Green"))
        data.containerName = containerTextField.text!
        data.createdDate = formatter.string(from: datePicker.date)
        data.plantId = plantId
        data.plantName = choosenPlant.text!
        data.dayTilHarvest = 60
        data.sensorStatus = #imageLiteral(resourceName: "Alert - Green")
        
        if data.containerName == "" || data.createdDate == "" || data.plantId == "" || data.plantName == "Choose Plant"  {
            let alert = UIAlertController(title: "Failed", message: "All data must be filled", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
        } else {
            let check = StoreContainerData(data: data)
            if check == true {
                let alert = UIAlertController(title: "Success", message: "Sensor Successfully connected", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: {(UIAlertAction) -> Void in
                    self.navigationController?.popToRootViewController(animated: true)
                }))
                
                self.present(alert, animated: true, completion: nil)
            } else {
                let alert = UIAlertController(title: "Failed", message: "Please try again later", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    @objc func editNow() {
        formatter.dateFormat = "dd MMMM yyyy"
        let date = Date()
        
        var data: DataContainer = DataContainer(containerName: "", plantId: "", plantName: "", createdDate: "", updatedAt: "", dayTilHarvest: 0, sensorStatus: #imageLiteral(resourceName: "Alert - Green"))
        data.containerName = containerTextField.text!
        data.createdDate = formatter.string(from: datePicker.date)
        data.plantId = plantId
        data.plantName = choosenPlant.text!
        data.updatedAt = formatter.string(from: date)
        
        if data.containerName == "" || data.createdDate == "" || data.plantId == "" || data.plantName == "Choose Plant" {
            let alert = UIAlertController(title: "Failed", message: "All data must be filled", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
        } else {
            let check = UpdateContainerData(data: data)
            if check == true {
                let alert = UIAlertController(title: "Success", message: "Data Successfully update", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: {(UIAlertAction) -> Void in
                    self.navigationController?.popToRootViewController(animated: true)
                }))
                
                self.present(alert, animated: true, completion: nil)
            } else {
                let alert = UIAlertController(title: "Failed", message: "Please try again later", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    @objc func goChoosePlant() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ChoosePlant") as! ChoosePlant
        self.present(vc, animated: true, completion: nil)
    }
}
