//
//  Helper.swift
//  hydrowatch
//
//  Created by Dicky Geraldi on 01/11/20.
//

import Foundation
import CoreData
import UIKit

func StoreContainerData(data: DataContainer) -> Bool {
    let image = data.sensorStatus.pngData()
    
    guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return false }
    
    let context = appDelegate.persistentContainer.viewContext
    
    let containerEntity = NSEntityDescription.entity(forEntityName: "ContainerData", in: context)!
    
    let container = NSManagedObject(entity: containerEntity, insertInto: context)
    
    container.setValue(data.containerName, forKey: "containerName")
    container.setValue(data.plantId, forKey: "plantId")
    container.setValue(data.plantName, forKey: "plantName")
    container.setValue(data.createdDate, forKey: "createdDate")
    container.setValue(data.dayTilHarvest, forKey: "dayTilHarvest")
    container.setValue(image, forKey: "sensorStatus")
    
    do {
        
        try context.save()
        return true
        
    } catch let error as NSError {
        
        print("Gagal save context \(error), \(error.userInfo)")
        return false
        
    }
}

func DeleteContainerData(plantId: String) -> Bool {
    guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return false }
    
    let managedContext = appDelegate.persistentContainer.viewContext
    
    let fetchRequest:NSFetchRequest<NSFetchRequestResult> = NSFetchRequest.init(entityName: "ContainerData")
    fetchRequest.predicate = NSPredicate(format: "plantId = %@", plantId)
    
    do {
        
        let dataToDelete = try managedContext.fetch(fetchRequest)[0] as! NSManagedObject
        managedContext.delete(dataToDelete)
        
        try managedContext.save()
        
        return true
        
    } catch let error as NSError {
        
        print("Gagal save context \(error), \(error.userInfo)")
        return false
        
    }
}

func UpdateContainerData(data: DataContainer) -> Bool {
    let image = data.sensorStatus.pngData()
    
    guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return false }
    
    let managedContext = appDelegate.persistentContainer.viewContext
    
    let fetchRequest:NSFetchRequest<NSFetchRequestResult> = NSFetchRequest.init(entityName: "ContainerData")
    fetchRequest.predicate = NSPredicate(format: "plantId = %@", data.plantId)
    
    do {
        
        let fetch = try managedContext.fetch(fetchRequest)
        let dataToUpdate = fetch[0] as! NSManagedObject
    
        dataToUpdate.setValue(data.containerName, forKey: "containerName")
        dataToUpdate.setValue(data.plantName, forKey: "plantName")
        dataToUpdate.setValue(data.createdDate, forKey: "createdDate")
        dataToUpdate.setValue(data.updatedAt, forKey: "updatedAt")
        dataToUpdate.setValue(data.dayTilHarvest, forKey: "dayTilHarvest")
        dataToUpdate.setValue(image, forKey: "sensorStatus")
        
        try managedContext.save()
        return true
        
    } catch let error as NSError {
        
        print("Gagal save context \(error), \(error.userInfo)")
        return false
        
    }
}

func GetContainerData() -> [DataContainer] {
    var containerData: [DataContainer] = []
    guard let appDel = UIApplication.shared.delegate as? AppDelegate else { return containerData }
    let context = appDel.persistentContainer.viewContext
    
    let fetch = NSFetchRequest<NSFetchRequestResult>(entityName: "ContainerData")
    let sort = NSSortDescriptor(key: "createdDate", ascending: false)
    fetch.sortDescriptors = [sort]
    
    do {
        
        let result = try context.fetch(fetch)
        
        for data in result as! [NSManagedObject] {
            containerData.append(DataContainer(containerName: data.value(forKey: "containerName") as! String, plantId: data.value(forKey: "plantId") as! String, plantName: data.value(forKey: "plantName") as! String, createdDate: data.value(forKey: "createdDate") as! String, updatedAt: data.value(forKey: "updatedAt") as? String ?? "", dayTilHarvest: data.value(forKey: "dayTilHarvest") as! Int16, sensorStatus: data.value(forKey: "sensorStatus") as? UIImage ?? #imageLiteral(resourceName: "Alert - Green")))
        }
        
    } catch {
        
        print("Failed to get data")
        
    }
    
    return containerData
}
