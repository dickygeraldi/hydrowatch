//
//  ScanQR.swift
//  hydrowatch
//
//  Created by Dicky Geraldi on 23/10/20.
//

import UIKit
import AVFoundation

class ScanQR: UIViewController, AVCaptureMetadataOutputObjectsDelegate {

    @IBOutlet weak var videoPreview: UIView!
    
    var dataQr: String = ""
    let avCaptureSession = AVCaptureSession()
    
    enum error: Error {
        case noCameraAvailable
        case videoInputFail
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "Create Container"
    }
    
    override func viewDidAppear(_ animated: Bool) {
        do {
            try ScanQr()
        } catch {
            print("Failed to scan QR")
        }
    }
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        if metadataObjects.count > 0 && metadataObjects != nil {
            let mechineReadableCode = metadataObjects[0] as! AVMetadataMachineReadableCodeObject
            if mechineReadableCode.type == AVMetadataObject.ObjectType.qr {
                avCaptureSession.stopRunning()
                let alert = UIAlertController(title: "Success", message: "Sensor Successfully connected", preferredStyle: .alert)
                        
                alert.addAction(UIAlertAction(title: "Next", style: .default, handler: {(UIAlertAction) -> Void in
                    self.dataQr = mechineReadableCode.stringValue!
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "CreateContainerVC") as! CreateContainerVC
                    vc.plantId = self.dataQr
                    self.navigationController?.pushViewController(vc, animated: true)
                }))
                
                self.present(alert, animated: true, completion: nil)
            }
        }
    }

    func ScanQr() throws {
        
        if avCaptureSession.inputs.isEmpty {
            guard let avCaptureDevice = AVCaptureDevice.default(for: AVMediaType.video) else {
                    print("No camera")
                    throw error.noCameraAvailable
            }
            
            guard let avCaptureInput = try? AVCaptureDeviceInput(device: avCaptureDevice) else {
                print("Failed to init camera")
                throw error.videoInputFail
            }
            
            let avCaptureMetadataOutput = AVCaptureMetadataOutput()
            
            avCaptureMetadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            
            avCaptureSession.addInput(avCaptureInput)
            avCaptureSession.addOutput(avCaptureMetadataOutput)
            avCaptureMetadataOutput.metadataObjectTypes = [AVMetadataObject.ObjectType.qr]
        }
        
        let avCaptureVideoPreview = AVCaptureVideoPreviewLayer(session: avCaptureSession)
        avCaptureVideoPreview.videoGravity = .resizeAspectFill
        avCaptureVideoPreview.frame = videoPreview.bounds
        
        self.videoPreview.layer.addSublayer(avCaptureVideoPreview)
        
        avCaptureSession.startRunning()
    }
}
